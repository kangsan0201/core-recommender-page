const express = require('express')
const router = express.Router()
const ejs = require('ejs')
const rp = require('request-promise')
const serverAddr = process.env.BESPOKE_SERVER_ADDR
const Promise = require('bluebird')

router.get('/', (req, res, next) => {
  res.render('index', { title: 'Bespoke Offering', list: [], teamStats: [] })
})


router.post('/bespoke', async (req, res) => {
  const title = 'Bespoke Offering'
  const { userId, currDate, currTime } = req.body
  const body = {
    userId,
    startDate: `${currDate}T${currTime}:00`
  }
  console.log('userId, currDate, currTime', userId, currDate, currTime)
  console.log('body', JSON.stringify(body))

  const bespokeOptions = {
    method: 'POST',
    uri: serverAddr + '/bespoke',
    body,
    json: true
  }

  const statsTeamOptions = {
    method: 'POST',
    uri: serverAddr + '/stats/team',
    body,
    json: true
  }

  try {
    const bespokeResp = await rp(bespokeOptions)
    console.log('/bespoke resp\n', JSON.stringify(bespokeResp, null, 4))

    const teamStatsResp = await rp(statsTeamOptions)
    console.log('/stats/team resp\n', JSON.stringify(teamStatsResp, null, 4))
    res.render('index', { title, list: bespokeResp.events, teamStats: teamStatsResp.stats })

  } catch(err) {
    console.log('POST /bespoke error', err)
    res.render('index', { title, list: [], teamStats: [] })
  }
})


router.get('/config', async (req, res, next) => {
  const title = 'Bespoke Config'
  const options = {
    uri: serverAddr + '/config/model',
    json: true
  }

  try {
    const resp = await rp(options)
    console.log('/config/model resp\n', JSON.stringify(resp, null, 4))
    res.render('config', { title, models: resp['model-config'] })

  } catch(err) {
    console.log('GET /config/model error', err)
    res.render('config', { title })
  }
})


router.post('/config/model', async (req, res) => {
  const title = 'Bespoke Config'
  const { descriptionList, periodDaysList, eventCountList } = req.body
  console.log(req.body)

  const configs = descriptionList.map((ele, idx) => ({
    id: idx + 1,
    description: ele,
    period_days: periodDaysList[idx],
    event_count: eventCountList[idx]
  }))
  const body = { 'model-config': configs }

  console.log('body', JSON.stringify(body))

  const options = {
    method: 'PUT',
    uri: serverAddr + '/config/model',
    body,
    json: true
  }

  try {
    const resp = await rp(options)
    console.log('PUT /config/model resp\n', JSON.stringify(resp, null, 4))
    res.render('config', { title, models: resp['model-config'] })

  } catch(err) {
    console.log('/config/model error', err)
    res.render('config', { title, models: configs })
  }
})


router.get('/activity', async (req, res, next) => {
  const title = 'User activities'
  const { userId, currDate, currTime } = req.query

  if (!userId) {
    res.render('activity', { title, activities: [] })
  }

  const startDate = `${currDate}T${currTime}:00`

  const options = {
    uri: serverAddr + `/activity?userId=${userId}&startDate=${startDate}&eventMaxCount=${5}`,
    json: true
  }

  try {
    const resp = await rp(options)
    console.log('/activity resp\n', JSON.stringify(resp, null, 4))
    res.render('activity', { title, activities: resp.activities })

    globalActivityUserId = userId

  } catch(err) {
    console.log('/activity error', err)
    res.render('activity', { title, activities: [] })
  }
})


router.post('/activity', async (req, res, next) => {
  const title = 'User activities'
  const { userId, checkList } = req.body
  console.log('--- checkList:', JSON.stringify(checkList, null, 4), '\nuserId:', userId)

  checkList.forEach((ele, idx) => {
    checkList[idx] = parseInt(ele)
  })

  const activities = checkList.map((ele, idx) => ({
    activityId: 1,
    userId,
    sportId: globalActivityEvents[ele].sport_id,
    competitionId: globalActivityEvents[ele].competition_id,
    eventId: globalActivityEvents[ele].event_id,
    homeTeamId: globalActivityEvents[ele].home_team_id,
    awayTeamId: globalActivityEvents[ele].away_team_id,
    competitionName: globalActivityEvents[ele].competition_name,
    homeTeamName: globalActivityEvents[ele].home_team_name,
    awayTeamName: globalActivityEvents[ele].away_team_name
  }))

  Promise.mapSeries(activities, async (item, idx, len) => {

    try {
      const options = {
        method: 'POST',
        uri: serverAddr + '/activity',
        body: item,
        json: true
      }

      const resp = await rp(options)
      console.log('POST /activity resp\n', JSON.stringify(resp, null, 4))

    } catch(err) {
      console.log('POST /bespoke error', err)
      res.render('index', { title, list: [], teamStats: [] })
    }
  })

  res.render('events', { title, events: globalActivityEvents })
})

let globalActivityEvents = []
let globalActivityUserId = 0


router.get('/events', async (req, res, next) => {
  const title = 'Events'
  const { currDate, currTime } = req.query

  if (!currDate && !currTime) {
    res.render('events', { title, events: [] })
  }

  const startDate = `${currDate}T${currTime}:00`

  const options = {
    uri: serverAddr + `/event?startDate=${startDate}`,
    json: true
  }

  try {
    const resp = await rp(options)
    console.log('/event resp\n', JSON.stringify(resp, null, 4))
    res.render('events', { title, events: resp.events })

    globalActivityEvents = resp.events

  } catch(err) {
    console.log('/event error', err)
    res.render('events', { title, events: [] })
  }
})


router.get('/neighbors', async (req, res, next) => {
  const title = 'Neighbors'

  const { userId, currDate, currTime } = req.query

  if (!userId) {
    res.render('neighbors', { title, neighbors: [] })
  }

  const options = {
    uri: serverAddr + `/neighbors?userId=${userId}`,
    json: true
  }

  try {
    const resp = await rp(options)
    console.log('/neighbors resp\n', JSON.stringify(resp, null, 4))
    const neighbors = resp.neighbors.map((item, idx) => ({
      userId: Object.keys(item)[0],
      similarity: Object.values(item)[0]
    }))
    console.log('/neighbors\n', JSON.stringify(neighbors, null, 4))

    res.render('neighbors', { title, neighbors })

    globalActivityUserId = userId

  } catch(err) {
    console.log('/neighbors error', err)
    res.render('neighbors', { title, neighbors: [] })
  }
})


module.exports = router

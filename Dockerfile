FROM circleci/node:8.12.0

ENV NODE_ENV production
ENV BESPOKE_SERVER_ADDR http://bespoke-test-01.appspot.com
ENV PORT 8080

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./package.json /usr/src/app/

RUN cd /usr/src/app/ && npm install

COPY ./usr/src/app/ /usr/src/app/app

CMD npm start

EXPOSE 8080
